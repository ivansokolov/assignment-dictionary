EXEC = dict

ASM = nasm
ASMFLAGS = -felf64 -g

LD = ld
LDFLAGS = -g

.SUFFIXES:
.PHONY: all clean run
.DEFAULT: all

all: $(EXEC)

clean:
	rm -f $(EXEC) *.o

run: $(EXEC)
	./$(EXEC)

$(EXEC): main.o dict.o lib.o
	$(LD) $(LDFLAGS) -o $@ $^

main.o: colon.inc words.inc
dict.o: colon.inc

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
