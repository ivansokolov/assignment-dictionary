; vim: set ft=nasm sw=4 ts=4 et:

    ; Add an entry header to the linked list. Anything that follows
    ; the entry header is considered the value
    ; %1 is a string, no longer than 255 characters. It is the key
    ; %2 is an identifier, the name for a label to the value
%macro COLON 2
%%header_begin:

%ifdef LIST_LAST
%%next:     dq LIST_LAST
%else
%%next:     dq 0
%endif
%define LIST_LAST %%header_begin

dq %%header_end-%%header_begin  ; header length
db %1, 0                        ; key

%%header_end:
%2:
%endmacro

    ; End the static linked list. The parameter will become a token
    ; that points to the (header of) first element of the list
%macro LIST_END 1
%xdefine %1 LIST_LAST
%undef LIST_LAST
%endmacro

    ; When REG points to an entry header, make it point to key
%define LIST_GET_KEY_PTR(REG) add REG, 16
    ; When REG points to an entry header, make it point to value
%define LIST_GET_VALUE_PTR(REG) add REG, qword [REG + 8]
    ; When REG points to an entry header, make it point to next
%define LIST_GET_NEXT_PTR(REG) mov REG, qword [REG]
