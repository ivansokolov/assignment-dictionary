; vim: set ft=nasm sw=4 ts=4 et:

%include "colon.inc"

    ; Find word in dictionary
    ;   rdi  --  null-terminated string
    ;   rsi  --  pointer to head of dictionary
    ; Returns:
    ;   On success, pointer to entry header (! not value !)
    ;   On failure, 0
global find_word
extern string_equals

section .text

find_word:
    push r11
    push r12

    mov r11, rdi
    mov r12, rsi

.loop_start:
    test r12, r12
    jz .ret0

    mov rdi, r12
    LIST_GET_KEY_PTR(rdi)
    mov rsi, r11
    call string_equals
    test rax, rax
    jnz .ret
    LIST_GET_NEXT_PTR(r12)
    jmp .loop_start

.ret0:
    xor r12, r12
.ret:
    mov rax, r12
    pop r12
    pop r11
    ret

