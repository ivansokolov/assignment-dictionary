; vim: set ft=nasm sw=4 ts=4 et :

    ; Exit with exitcode rdi
global exit
    ; Return length of 0-terminated string, pointed to by rdi, to the descriptor rsi
global string_length
    ; Print a zero-terminated string, pointed to by rdi
global print_string
    ; Check whether two zero-terminated strings, pointed to by rdi & rsi, are equal
global string_equals
    ; Read a word into a buffer, pointed to by rdi, of size rsi, skipping whitespace
    ;    and zero-terminating the word
    ; rax <- buffer address
    ; rdi <- word length
global read_word

SYS_READ  equ 0
SYS_WRITE equ 1
SYS_EXIT  equ 60

FD_STDIN  equ 0
FD_STDOUT equ 1
FD_STDERR equ 2

section .text

exit:
    mov eax, SYS_EXIT
    syscall

string_length:
    xor rcx, rcx
    xor rax, rax
    not rcx
    repne scasb
    mov rax, rcx
    not rax
    dec rax
    ret

print_string:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax
    mov eax, SYS_WRITE
    syscall
    ret

string_equals:
.loop:
    mov al, [rsi]
    cmp al, [rdi]
    jne .different
    inc rdi
    inc rsi
    test al, al
    jnz .loop
    mov eax, 1
    ret
.different:
    xor rax, rax
    ret

read_char:
    push qword 0
    mov rdi, FD_STDIN
    mov rsi, rsp
    mov edx, 1
    mov eax, SYS_READ
    syscall
    mov rax, [rsp]
    add rsp, 8
    ret

read_word:
%macro JUMP_IF_WHITESPACE 1
    cmp al, 0x20
    je %1
    cmp al, 0x09
    je %1
    cmp al, 0x0A
    je %1
%endmacro
    push rbx  ; word length
    push r12  ; buffer address
    push r13  ; buffer space remaining
    mov r12, rdi
    mov r13, rsi
.skip_whitespace:
    call read_char
    test al, al
    jz .word_end
    JUMP_IF_WHITESPACE .skip_whitespace
.loop:
    dec r13
    jz .buffer_exhausted
    mov [r12 + rbx], al
    inc rbx
    call read_char
    test al, al
    jz .word_end
    JUMP_IF_WHITESPACE .word_end
    jmp .loop
.word_end:
    mov byte [r12 + rbx], 0
    mov rax, r12
    mov rdx, rbx
    jmp .ret
.buffer_exhausted:
    xor rax, rax
.ret:
    pop r13
    pop r12
    pop rbx
    ret
%unmacro JUMP_IF_WHITESPACE 1

