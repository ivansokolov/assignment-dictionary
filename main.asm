; vim: set ft=nasm sw=4 ts=4 et:

%include "colon.inc"

global _start

extern find_word
extern exit
extern print_string
extern read_word

FD_STDIN  equ 0
FD_STDOUT equ 1
FD_STDERR equ 2

section .bss ; =================================================================

buf:        resb 256
buf_end:

section .rodata ; ==============================================================

%include "words.inc"
LIST_END dict

msg_notfound:   db "Key not found", 10, 0

section .text ; ================================================================

_start:
    mov rdi, buf
    mov rsi, buf_end-buf
    call read_word

    mov rdi, buf
    mov rsi, dict
    call find_word

    test rax, rax
    jnz .found_entry
    mov rdi, msg_notfound
    mov rsi, FD_STDERR
    call print_string
    mov rdi, 1
    jmp exit

.found_entry:
    LIST_GET_VALUE_PTR(rax)
    mov rdi, rax
    mov rsi, FD_STDOUT
    call print_string
    xor rdi, rdi
    jmp exit

